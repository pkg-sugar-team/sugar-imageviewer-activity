# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-24 17:39+1100\n"
"PO-Revision-Date: 2017-12-06 09:30+0000\n"
"Last-Translator: mbouzada <mbouzada@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.5.1.1\n"
"X-POOTLE-MTIME: 1512552605.000000\n"

#: activity/activity.info:2
msgid "Image Viewer"
msgstr "Visor de imaxes"

#: activity/activity.info:3
msgid ""
"An easy way and fast way to see your images! You can also change the way "
"your image looks with zoom, rotate, size change and so on."
msgstr ""
"Unha maneira fácil e rápida de ver as súas imaxes! Tamén pode cambiar o "
"aspecto da súa imaxe facendo zoom, rotándoa, cambiando o seu tamaño e demais."

#: ImageViewerActivity.py:202
msgid "No image"
msgstr "Ningunha imaxe"

#: ImageViewerActivity.py:213
msgid "Choose an image"
msgstr "Escolla unha imaxe"

#: ImageViewerActivity.py:245
msgid "Please wait"
msgstr "Agarde un intre, por favor"

#: ImageViewerActivity.py:246
msgid "Starting connection..."
msgstr "Iniciando a conexión..."

#: ImageViewerActivity.py:301
msgid "Zoom out"
msgstr "Diminuír"

#: ImageViewerActivity.py:307
msgid "Zoom in"
msgstr "Aumentar"

#: ImageViewerActivity.py:313
msgid "Fit to window"
msgstr "Axustar á xanela"

#: ImageViewerActivity.py:319
msgid "Original size"
msgstr "Tamaño orixinal"

#: ImageViewerActivity.py:325
msgid "Fullscreen"
msgstr "Pantalla completa"

#: ImageViewerActivity.py:335
msgid "Rotate anticlockwise"
msgstr "Tamaño orixinal"

#: ImageViewerActivity.py:342
msgid "Rotate clockwise"
msgstr "Rotar no sentido das agullas do reloxo"

#: ImageViewerActivity.py:353
msgid "Previous Image"
msgstr "Imaxe anterior"

#: ImageViewerActivity.py:361
msgid "Next Image"
msgstr "Seguinte imaxe"

#: ImageViewerActivity.py:650
msgid "Receiving image..."
msgstr "Recibindo a imaxe..."
